### Architecture

This project is based in the MVP architecture.

* Model: In this case only contains domain classes with parsing information and parsing adapters

* Presenter: Each fragment has it's own presenter with it's own Contract class. The Contract classes contains two interfaces that defines the methods available for the interaction between the Presenter and the View (in this case the fragment)  

* View: I would have liked to dedicate more time to the design... There are a couple very simple custom views, adapters, a "master" activity and two fragments; Master and Detail.

### Libraries used in the project 

* OkHttp3 (Square)
* Gson (Google)
* Butterknife
* Picasso (Square)

### Patterns

* Since it's a very small project, I thought it wouldn't have made too much sense to add forced patterns to it. Besides MVP and the Singletons in the presenters (that could be replaced with Dagger) there's not much to see.