package solstice.com.solsticechallenge.view.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import solstice.com.solsticechallenge.R;
import solstice.com.solsticechallenge.model.Contact;
import solstice.com.solsticechallenge.presenter.MasterPresenter;
import solstice.com.solsticechallenge.presenter.contracts.MasterContract;
import solstice.com.solsticechallenge.view.activities.MainActivity;
import solstice.com.solsticechallenge.view.adapters.ContactsAdapter;
import solstice.com.solsticechallenge.view.custom.StateView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MasterFragment extends Fragment implements MasterContract.View, ContactsAdapter.ContactListener, StateView.StateViewListener{

    MasterPresenter presenter;

    @BindView(R.id.fragment_master_state)
    StateView v_state;
    @BindView(R.id.fragment_master_list)
    RecyclerView v_list;

    public MasterFragment() {
        presenter = MasterPresenter.getInstance();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_master, container, false);
        ButterKnife.bind(this,v);
        initUI();
        presenter.suscribe(this);
        return v;
    }

    private void initUI() {
        v_list.setHasFixedSize(true);
        v_list.setLayoutManager(new LinearLayoutManager(this.getContext()));
        v_list.addItemDecoration(new DividerItemDecoration(v_list.getContext(),DividerItemDecoration.VERTICAL));
        v_state.loading();
        v_state.setListener(this);
    }

    @Override
    public void setContactList(Contact[] contacts) {
        v_list.setAdapter(new ContactsAdapter(getContext(),contacts,this));
        v_state.ok();
    }

    @Override
    public void onError() {
        v_state.error(R.string.error_generic);
    }

    @Override
    public void onContactClicked(Contact contact) {
        MainActivity activity = (MainActivity) getActivity();
        DetailFragment fragment = new DetailFragment();
        fragment.setContact(contact);
        activity.fragmentTransaction(fragment);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unsuscribe();
    }

    @Override
    public void onRetryClicked() {
        v_state.loading();
        presenter.refresh();
    }
}
