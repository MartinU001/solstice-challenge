package solstice.com.solsticechallenge.view.custom;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import solstice.com.solsticechallenge.R;

/**
 * Created by hp on 19-Jun-17.
 */

public class DataDescriptionView extends FrameLayout{

    @BindView(R.id.custom_view_datadesc_data)
    TextView v_data;
    @BindView(R.id.custom_view_datadesc_description)
    TextView v_description;

    public DataDescriptionView(@NonNull Context context) {
        super(context);
        init();
    }

    public DataDescriptionView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DataDescriptionView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        View view = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_view_datadesc,this);
        ButterKnife.bind(this,view);
    }

    public void setData(String data){
        v_data.setText(data);
    }

    public void setDescription(String description){
        v_description.setText(description);
    }
}
