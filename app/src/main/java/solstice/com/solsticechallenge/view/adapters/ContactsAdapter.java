package solstice.com.solsticechallenge.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import solstice.com.solsticechallenge.R;
import solstice.com.solsticechallenge.model.Contact;

/**
 * Created by hp on 15-Jun-17.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private Context context;
    private Contact[] contacts;
    private ContactListener listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        @BindView(R.id.item_contact_img)
        public ImageView c_img;
        @BindView(R.id.item_contact_name)
        public TextView c_name;
        @BindView(R.id.item_contact_phone)
        public TextView c_phone;

        public ViewHolder(View v) {
            super(v);
            view=v;
            ButterKnife.bind(this,v);
        }
    }

    public ContactsAdapter(Context context, Contact[] contacts, ContactListener listener) {
        this.context = context;
        this.contacts = contacts;
        this.listener = listener;
    }

    @Override
    public ContactsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Contact current_contact = contacts[position];
        holder.c_name.setText(current_contact.name);
        holder.c_phone.setText(current_contact.phone.home);
        Picasso.with(context)
                .load(current_contact.smallImageURL)
                .placeholder(R.drawable.ic_account_circle_grey600_48dp)
                .into(holder.c_img);
        holder.view.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
                                                   listener.onContactClicked(current_contact);
                                               }
                                           }
        );
    }

    @Override
    public int getItemCount() {
        return contacts.length;
    }

    public interface ContactListener{
        void onContactClicked(Contact contact);
    }
}
