package solstice.com.solsticechallenge.view.fragments;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import solstice.com.solsticechallenge.R;
import solstice.com.solsticechallenge.model.Address;
import solstice.com.solsticechallenge.model.Contact;
import solstice.com.solsticechallenge.presenter.DetailPresenter;
import solstice.com.solsticechallenge.presenter.contracts.DetailContract;
import solstice.com.solsticechallenge.view.custom.DataDescriptionView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment implements DetailContract.View{

    DetailPresenter presenter;

    @BindView(R.id.fragment_detail_img)
    ImageView c_image;
    @BindView(R.id.fragment_detail_name)
    TextView c_name;
    @BindView(R.id.fragment_detail_company)
    TextView c_company;
    @BindView(R.id.fragment_details_phones_work)
    DataDescriptionView c_phone_work;
    @BindView(R.id.fragment_details_phones_home)
    DataDescriptionView c_phone_home;
    @BindView(R.id.fragment_details_phones_mobile)
    DataDescriptionView c_phone_mobile;
    @BindView(R.id.fragment_details_address_work)
    DataDescriptionView c_address_work;
    @BindView(R.id.fragment_details_birthday)
    DataDescriptionView c_birthday;
    @BindView(R.id.fragment_details_email)
    DataDescriptionView c_email;
    @BindView(R.id.fragment_detail_fav)
    ImageView c_fav;

    public DetailFragment() {
        presenter = DetailPresenter.getInstance();
    }

    public void setContact(Contact contact){
        presenter.setContact(contact);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this,v);
        initUI();
        presenter.suscribe(this);
        return v;
    }

    private void initUI() {

    }

    @Override
    public void showContact(Contact contact) {
        c_name.setText(contact.getName());
        c_company.setText(contact.getCompany());
        setupPhoto(contact.getSmallImageURL(),contact.getLargeImageURL());
        setup_phones(contact.getPhone().work,contact.getPhone().home,contact.getPhone().mobile);
        setupAddress(contact.getAddress());
        c_birthday.setData(contact.getReadableBirthdate());
        c_email.setData(contact.getEmail());
        c_email.setDescription(getResources().getString(R.string.work));
        c_fav.setVisibility(contact.isFavorite() ? View.VISIBLE : View.GONE);
    }

    private void setupPhoto(String cached, String newone){
        Picasso.with(getContext()).load(cached).into(c_image);
        Picasso.with(getContext()).load(newone).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                c_image.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    private void setup_phones(String work, String home, String mobile){
        boolean workEmpty = work==null || work.isEmpty();
        c_phone_work.setVisibility(workEmpty ? View.GONE : View.VISIBLE);
        c_phone_work.setData(work);
        c_phone_work.setDescription(getResources().getString(R.string.work));

        boolean homeEmpty = home==null || home.isEmpty();
        c_phone_home.setVisibility(homeEmpty ? View.GONE : View.VISIBLE);
        c_phone_home.setData(home);
        c_phone_home.setDescription(getResources().getString(R.string.home));

        boolean mobileEmpty = mobile==null || mobile.isEmpty();
        c_phone_mobile.setVisibility(mobileEmpty ? View.GONE : View.VISIBLE);
        c_phone_mobile.setData(mobile);
        c_phone_mobile.setDescription(getResources().getString(R.string.mobile));
    }

    private void setupAddress(Address address){
        String content = address.street + ", " + address.city + ", " + address.zip;
        c_address_work.setData(content);
        c_address_work.setDescription(getResources().getString(R.string.work));
    }
}
