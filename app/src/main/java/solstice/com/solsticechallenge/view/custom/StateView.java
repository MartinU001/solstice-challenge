package solstice.com.solsticechallenge.view.custom;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import solstice.com.solsticechallenge.R;

/**
 * Created by hp on 18-Jun-17.
 */

public class StateView extends FrameLayout {

    StateViewListener listener;

    @BindView(R.id.custom_view_states_loadingContainer)
    View v_loading;
    @BindView(R.id.custom_view_states_errorContainer)
    View v_error;

    @BindView(R.id.custom_view_states_error)
    TextView tvt_error;
    @BindView(R.id.custom_view_states_retry)
    View crd_retry;

    public StateView(@NonNull Context context) {
        super(context);
        init();
    }

    public StateView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StateView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        View view = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_view_states,this);
        ButterKnife.bind(this,view);
        setVisibility(GONE);
        crd_retry.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null)
                    listener.onRetryClicked();
            }
        });
    }

    public void setListener(StateViewListener listener){
        this.listener = listener;
    }

    public void error(int error_message){
        String message = getContext().getResources().getString(error_message);
        error(message);
    }

    public void error(String error_message){
        setVisibility(VISIBLE);
        v_loading.setVisibility(GONE);
        v_error.setVisibility(VISIBLE);
        tvt_error.setText(error_message.trim());
    }

    public void loading(){
        setVisibility(VISIBLE);
        v_loading.setVisibility(VISIBLE);
        v_error.setVisibility(GONE);
    }

    public void ok(){
        setVisibility(GONE);
    }

    public interface StateViewListener{
        void onRetryClicked();
    }


}
