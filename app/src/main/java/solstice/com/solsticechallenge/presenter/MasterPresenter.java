package solstice.com.solsticechallenge.presenter;

import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import solstice.com.solsticechallenge.model.Contact;
import solstice.com.solsticechallenge.model.network.deserializers.BooleanAdapter;
import solstice.com.solsticechallenge.model.network.deserializers.DateAdapter;
import solstice.com.solsticechallenge.model.network.utils.MainThreadCallback;
import solstice.com.solsticechallenge.presenter.contracts.MasterContract;

/**
 * Created by hp on 15-Jun-17.
 */

public class MasterPresenter implements MasterContract.Presenter {

    private static MasterPresenter instance;

    private MasterPresenter() {
    }

    public static MasterPresenter getInstance() {
        if (instance == null)
            instance = new MasterPresenter();
        return instance;
    }

    final String CONTACTS_URL = "https://s3.amazonaws.com/technical-challenge/Contacts_v2.json";

    MasterContract.View view;
    Contact[] contacts;

    @Override
    public void suscribe(MasterContract.View view) {
        this.view = view;
        onTakeView();
    }

    @Override
    public void unsuscribe() {
        this.view = null;
    }

    @Override
    public void refresh() {
        loadContacts();
    }

    private void onTakeView() {
        if (contacts == null)
            loadContacts();
        else
            view.setContactList(contacts);
    }

    private void loadContacts() {

        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder().url(CONTACTS_URL).build();
        client.newCall(request).enqueue(new MainThreadCallback() {

            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        e.printStackTrace();
                        if (view != null)
                            view.onError();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                GsonBuilder builder = new GsonBuilder();
                builder.registerTypeAdapter(boolean.class, new BooleanAdapter());
                builder.registerTypeAdapter(Date.class, new DateAdapter());
                Gson gson = builder.create();
                contacts = gson.fromJson(response.body().string(), Contact[].class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (view != null)
                            view.setContactList(contacts);
                    }
                });

            }
        });
    }
}
