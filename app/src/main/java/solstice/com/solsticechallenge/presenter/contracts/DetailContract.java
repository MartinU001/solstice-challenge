package solstice.com.solsticechallenge.presenter.contracts;

import solstice.com.solsticechallenge.model.Contact;

public class DetailContract {

    public interface View {
        void showContact(Contact contact);
    }

    public interface Presenter {
        void suscribe(View view);
        void unsuscribe();
        void setContact(Contact contact);
    }

}
