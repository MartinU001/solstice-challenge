package solstice.com.solsticechallenge.presenter;

import solstice.com.solsticechallenge.model.Contact;
import solstice.com.solsticechallenge.presenter.contracts.DetailContract;

/**
 * Created by hp on 15-Jun-17.
 */

public class DetailPresenter implements DetailContract.Presenter {

    //Just a Singleton, could have used Dagger as well
    private static DetailPresenter instance;
    private DetailPresenter(){ }
    public static DetailPresenter getInstance(){
        if (instance==null)
            instance = new DetailPresenter();
        return instance;
    }

    Contact contact;
    DetailContract.View view;

    @Override
    public void suscribe(DetailContract.View view) {
        this.view=view;
        onTakeView();
    }

    @Override
    public void unsuscribe() {
        this.view = null;
    }

    @Override
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    private void onTakeView() {
        if (view != null)
            view.showContact(contact);
    }
}
