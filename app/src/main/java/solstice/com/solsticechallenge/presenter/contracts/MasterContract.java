package solstice.com.solsticechallenge.presenter.contracts;

import java.util.List;

import solstice.com.solsticechallenge.model.Contact;

public class MasterContract {

    public interface View {
        void setContactList(Contact[] contacts);
        void onError();
    }

    public interface Presenter {
        void suscribe(View view);
        void unsuscribe();
        void refresh();
    }

}
