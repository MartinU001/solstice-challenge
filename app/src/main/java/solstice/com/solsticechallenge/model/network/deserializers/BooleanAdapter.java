package solstice.com.solsticechallenge.model.network.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by hp on 15-Jun-17.
 */

public class BooleanAdapter implements JsonDeserializer<Boolean> {
    @Override
    public Boolean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String bool = json.getAsString().trim().toLowerCase();
        if (bool.equals("false") || bool.equals("0"))
            return false;
        else
            return true;
    }
}
