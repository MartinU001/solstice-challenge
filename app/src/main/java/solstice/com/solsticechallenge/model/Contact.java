
package solstice.com.solsticechallenge.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Contact {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("company")
    @Expose
    public String company;
    @SerializedName("favorite")
    @Expose
    public boolean favorite;
    @SerializedName("smallImageURL")
    @Expose
    public String smallImageURL;
    @SerializedName("largeImageURL")
    @Expose
    public String largeImageURL;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("website")
    @Expose
    public String website;
    @SerializedName("birthdate")
    @Expose
    public Date birthdate;
    @SerializedName("phone")
    @Expose
    public Phone phone;
    @SerializedName("address")
    @Expose
    public Address address;

    public String getName() {
        return name;
    }

    public String getCompany() {
        return company;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public String getSmallImageURL() {
        return smallImageURL;
    }

    public String getLargeImageURL() {
        return largeImageURL;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() {
        return website;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public String getReadableBirthdate() {
        SimpleDateFormat dt1 = new SimpleDateFormat("MMMM dd, yyyy");
        return dt1.format(birthdate);
    }

    public Phone getPhone() {
        return phone;
    }

    public Address getAddress() {
        return address;
    }
}
