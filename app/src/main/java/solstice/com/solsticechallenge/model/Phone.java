
package solstice.com.solsticechallenge.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Phone {

    @SerializedName("work")
    @Expose
    public String work;
    @SerializedName("home")
    @Expose
    public String home;
    @SerializedName("mobile")
    @Expose
    public String mobile;

}
